<?php

session_start();

if(file_exists("lib/funkcje.php")) require_once("lib/funkcje.php");

  if(!$_SESSION[email]){

    redirect('zaloguj.php');

  }

?>

<!DOCTYPE html>

<html lang="pl">

<?php 

    if(file_exists("header.php")) include ("header.php");

    ?>

<body>

     <div class="container">

          <?php 

          if(file_exists("nav.php")) include ("nav.php");

          if(file_exists("slider.php")) include ("slider.php");

          ?>

          <main>

           <article>

            <?php 

              echo "<h2> Witaj ".$_SESSION[imie]." ".$_SESSION[nazwisko]."!</h2><p> Po prawej stronie znajdują się dostępne opcje.</p>

              <p> Życzymy miłego dnia! </p>"?>

              </article>

              <?php if(file_exists("menu.php")) include ("menu.php"); ?> 

          </main>

          <footer>

                <p>Copyright &copy; 2018</p>

            </footer>

    </div> 

</body>

</html>