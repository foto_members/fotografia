<?php
session_start();
if(file_exists("lib/funkcje.php")) require_once("lib/funkcje.php");
  if(!$_SESSION[email]){
    redirect('zaloguj.php');
  } else if($_SESSION[rola] != "1"){
    redirect('konto.php');
}
?>
<!DOCTYPE html>
<html lang="pl">
<?php 
    if(file_exists("header.php")) include ("header.php");
    ?>
<body>
     <div class="container">
          <?php 
         if(file_exists("nav.php")) include ("nav.php");
         if(file_exists("slider.php")) include ("slider.php");
         ?>
         <main>
         <article>
         <?php
         if(file_exists("config.php")) require_once("config.php");
         if(file_exists("lib/funkcje.php")) require_once("lib/funkcje.php");
         $query="SELECT orders.id, imie, nazwisko, telefon, date, typ, godzina FROM orders JOIN users ON orders.userID = users.id WHERE accepted = '0'";
         $wynik = mysqli_query($link,$query);
         se($link);
         echo "<h2>Rezerwacje oczekujące na akceptację :</h2>";
        
            if($wynik->num_rows > 0){
                           echo "<table class=\"table\">\n";
         echo "<tr>\n";
         echo " <th></th><th>Imię </th><th>Nazwisko</th><th>Numer telefonu </th><th>Data</th><th>Typ sesji</th><th>Godzina</th>\n";
         echo "</tr>\n";
         while($wiersz = mysqli_fetch_assoc($wynik)){
             echo "<tr>\n";
             echo "<td><a class=\"button\" href=\"rezerwacje_funkcje.php?id=".$wiersz[id]."&accepted=1\">Akceptuj</a>
             <a class=\"button\" href=\"rezerwacje_funkcje.php?id=".$wiersz[id]."&delete=1\">Odmów</a>
             </td><td>".$wiersz[imie]."</td><td>".$wiersz[nazwisko]."</td><td>".$wiersz[telefon]."</td><td>".$wiersz[date]."</td><td>".$wiersz[typ]."</td><td>".$wiersz[godzina]."</td>\n";
             echo "</tr>\n";
         }
         echo "</table>\n";
            }
             else {
                 echo "Brak rezerwacji.";
             }
             
             $query1="SELECT orders.id, imie, nazwisko, telefon, date, typ, godzina FROM orders JOIN users ON orders.userID = users.id WHERE accepted = '1'";
         $wynik1 = mysqli_query($link,$query1);
         se($link);
         echo "<h2>Rezerwacje potwierdzone :</h2>";
         
             if($wynik1->num_rows > 0){
                    echo "<table class=\"table\">\n";
             echo "<tr>\n";
             echo " <th></th><th>Imię </th><th>Nazwisko</th><th>Numer telefonu </th><th>Data</th><th>Typ sesji</th><th>Godzina</th>\n";
             echo "</tr>\n";
         while($wiersz1 = mysqli_fetch_assoc($wynik1)){
             echo "<tr>\n";
             echo "<td><a class=\"button\" href=\"rezerwacje_funkcje.php?id=".$wiersz1[id]."&delete=1\">Usuń</a>
             </td><td>".$wiersz1[imie]."</td><td>".$wiersz1[nazwisko]."</td><td>".$wiersz1[telefon]."</td><td>".$wiersz1[date]."</td><td>".$wiersz1[typ]."</td><td>".$wiersz1[godzina]."</td>\n";
             echo "</tr>\n";
         }
         echo "</table>\n";   
             } else {
                 echo "Brak rezerwacji.";
             }
             ?>
             </article>
          <?php if(file_exists("menu_a.php")) include ("menu_a.php"); ?> 
         </main>
              <footer>
                    <p>Copyright &copy; 2018 </p>
                </footer>
        </div> 
</body>
</html>