<?php 
  if(file_exists("../lib/funkcje.php")) require_once("../lib/funkcje.php");
  if(file_exists("../config.php")) require_once("../config.php");

  $link=mysqli_connect($host, $user, $password) or die("<p>Nie mogę połączyć się z serwerem.</p>\n");
  mysqli_select_db($link, $dbname) or die("<p>Nie mogę połączyć się z bazą danych.</p>\n");
  mysqli_query($link, "SET NAMES utf8");


  $query = "CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `date` date NOT NULL,
  `typ` varchar(35) NOT NULL,
  `godzina` varchar(10) NOT NULL,
  `accepted` int(1) NOT NULL DEFAULT '0'
);";

  mysqli_query($link, $query);
  se($link);

  redirect("../index.php");
        
?>