<?php 
session_start();
if(file_exists("lib/funkcje.php")) require_once("lib/funkcje.php");
if(!$_SESSION[email]){
    redirect('zaloguj.php');
  } else if($_SESSION[rola] == "1"){
    redirect('a_konto.php');
}
else {
   redirect('konto.php');
}
?>