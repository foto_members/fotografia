
<html lang="pl">
<?php 
    if(file_exists("header.php")) include ("header.php");
    ?>
<body>
     <div class="container">
          <?php 
          if(file_exists("header.php")) include ("header.php");
         ?>          
         <form role="form" action="logowanie.php" method="POST">
                        <fieldset>
                            <h2>Zaloguj się</h2>
                             <div class="alert-box">
                                <?php include('lib/error.php'); ?>
                            </div>
                            <div class="form-group">
                               <label for="email">Adres e-mail: </label> <input class="form-control" placeholder="e-mail" name="email" type="text" required>
                            </div>
                            <div class="form-group">
                               <label for="haslo">Hasło: </label> <input class="form-control" placeholder="Hasło" name="haslo" type="password" value="" required>
                            </div>
                            <button type="submit">Zaloguj się</button>  
                            <a class="btn btn-link" href="rejestracja.php">Zarejestruj się</a>  
                            <a href="index.php">Powrót</a>  
                        </fieldset>
                      </form>
        
            <footer>
                <p>Copyright &copy; 2018</p>
            </footer>
    </div>
    
</body>
</html>