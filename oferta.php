<!DOCTYPE html>
<html lang="pl">
<?php 
    if(file_exists("header.php")) include ("header.php");
    ?>
<body>
     <div class="container">
          <?php 
          if(file_exists("nav.php")) include ("nav.php");
          if(file_exists("slider.php")) include ("slider.php");
          ?>
        <main>
            <article>
                <h1>Co oferuję?</h1>
                <p>Dysponuję profesjonalnym sprzętem fotograficznym firmy Canon. Każde zlecenie wykonuję z największą starannością i pełnym zaangażowaniem, tak aby efekt końcowy był zgodny z wymaganiami klienta. Do zdjęć podchodzę z szacunkiem, dbając o estetykę i efekt.</p>
                <p>Moja oferta obejmuje fotografię okolicznościową i nie tylko. Wykonuję reportaże ślubne, chrztu, sesje indywidualne (plenerowe), plenery ślubne, rodzinne, dziecięce, ciążowe, narzeczeńskie, a także sesje biznesowe oraz fotografię wnętrz. Ofertę można dostosować do indywidualnych oczekiwań estetycznych i finansowych.</p>
                <p>Wszystkie fotografie poddawane są starannej obróbce. Wiecie, że obróbka jednego zdjęcia może trwać nawet 30 minut? Łatwo policzyć więc czas jaki poświęca się dodatkowo aby materiał był taki wyjątkowy. To nie tylko "pstrykanie" fotek na imprezie, ale selekcja, wyłonienie z setek plików tych najlepszych ujęć, które czynią reportaż spójnym, a zarazem ciekawym oraz obróbka, która sprawia, że zdjęcia mają to „coś” . 
                O oryginalności i wyglądzie Waszych zdjęć decyduje nie tylko umiejętność obsługi aparatu. Wszystko to by Wasze zdjęcia były inne niż pozostałe.</p> 
                <p>Jeśli szukasz fotografa, który za pomocą zdjęć stworzy opowieść, zatrzyma w kadrze to co najważniejsze napisz do mnie . 🙂</p> 
            </article>
            <aside>
                <section class="widget">
                    <h2>Wykonuje:</h2>
                    <ul>
                        <li>FOTOGRAFIE PORTRETOWE</li>
                        <li>FOTOGRAFIE ŚLUBNE</li>
                        <li>FOTOGRAFIE OKOLICZNOŚCIOWE</li>
                        <li>FOTOGRAFIE DZIECIĘCE</li>
                        <li>SESJE INDYWIDUALNE</li>
                        <li>ZDJĘCIA UŻYTKOWE</li>
                        <li>REPORTAŻE</li>
                    </ul> 
                </section>
            </aside>
         </main>
         <footer>
                <p>Copyright &copy; 2018 </p>
         </footer>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
    <script src="js/nivo-slider/jquery.nivo.slider.pack.js"></script>
    <script src="js/main.js"></script>
</body>
</html>