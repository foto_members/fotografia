<?php
session_start();
if(file_exists("lib/funkcje.php")) require_once("lib/funkcje.php");
  if(!$_SESSION[email]){
    redirect('zaloguj.php');
  }
?>
<!DOCTYPE html>
<html lang="pl">
<?php 
    if(file_exists("header.php")) include ("header.php");
    ?>
<body>
     <div class="container">
          <?php 
          if(file_exists("nav.php")) include ("nav.php");
          if(file_exists("slider.php")) include ("slider.php");
          ?>
          <main>
           <article>
             <form role="form" action="rezerwuj_post.php" method="POST">
                        <fieldset>
                            <h2>Rezerwacja terminu</h2>
                             <div class="alert-box">
                                <?php include('lib/error.php'); ?>
                            </div>
                            <div class="form-group">
                               <label for="date">Wybierz termin: </label> <input class="form-control" name="date" type="date" required>
                            </div>
                            <div class="form-group">
                               <label for="typ">Wybierz typ sesji: </label> <select name="typ" required>
                                    <option>SESJA ŚLUBNA</option>
                                    <option>SESJA OKOLICZNOŚCIOWA</option>
                                    <option>SESJA DZIECIĘCA</option>
                                    <option>SESJA INDYWIDUALNA</option>
                                </select>
                               <br> <label for="godzina">Podaj godzinę: </label><input class="form-control" placeholder="np. 16:00" name="godzina" type="text" required>
                            </div>
                            <button type="submit">Rezerwuj</button>    
                        </fieldset>
                      </form>
              </article>
              <?php if(file_exists("menu.php")) include ("menu.php"); ?> 
          </main>
          <footer>
                <p>Copyright &copy; 2018 - Michał Nawrocki</p>
            </footer>
    </div> 
</body>
</html>