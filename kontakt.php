<!DOCTYPE html>
<html lang="pl">
<?php 
    if(file_exists("header.php")) include ("header.php");
    ?>
<body>
     <div class="container">
          <?php 
          if(file_exists("nav.php")) include ("nav.php");
          if(file_exists("slider.php")) include ("slider.php");
          ?>
        <main>
        <article>
            <h1>Kontakt</h1>
            <p>Martyna Bogdańska - FOTOGRAFIA</p>
            <p>e-mail: martyna.bogdanska@wp.pl</p>
            <p>tel: 721 099 087</p>
        </article>
        <aside>
            <section class="widget">
                        <h3>Znajdź mnie na:</h3>
                        <ul>
                            <li><a href="https://www.facebook.com/martynabogdanskafotografia"><img src="images/fb.png" alt="???"><u>Facebook-u</u></a></li>
                            <li><a href="https://www.instagram.com/booogdzi/"><img src="images/insta.png" alt="???"><u>Instagramie</u></a></li>
                        </ul>
            </section>
            </aside>
        </main>
         <footer>
                <p>Copyright &copy; 2018 </p>
         </footer>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
    <script src="js/nivo-slider/jquery.nivo.slider.pack.js"></script>
    <script src="js/main.js"></script>
    
</body>
</html>