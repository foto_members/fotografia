<?php
session_start();
if(file_exists("config.php")) require_once("config.php");
if(file_exists("lib/funkcje.php")) require_once("lib/funkcje.php");

$login = normalize($link, $_SESSION[email]);
  $stareHaslo = hashPassword($_POST[stareHaslo]);
  $noweHaslo = normalize($link, $_POST[noweHaslo]);
  $noweHasloPotwierdzenie = normalize($link, $_POST[noweHasloPotwierdzenie]);
  
   if (!isset($login))
  {
    redirect('zmien_haslo.php?error=Nie można zmienić hasła.');
  }

  if (!isNullOrEmptyString($stareHaslo) && !isNullOrEmptyString($noweHaslo) && !isNullOrEmptyString($noweHasloPotwierdzenie)) {
    if ($noweHaslo != $noweHasloPotwierdzenie) {
      redirect('zmien_haslo.php?&error=Wpisane hasło nie jest identyczne.');
    }

    $query = "SELECT id, email, haslo from users where email like '".$login."'";
    $select = mysqli_query($link,$query);
    if($user=mysqli_fetch_assoc($select)){
      if ($stareHaslo != $user[haslo])
      {
        redirect('zmien_haslo.php?error=Stare hasło jest niepoprawne.');
      }
      else {
        $queryUpdate = "UPDATE users set haslo = '".hashPassword($noweHaslo)."' where id=".$user[id];
        $update = mysqli_query($link,$queryUpdate);
         
        redirect('zmiana_hasla_succes.php');
      }

      
    }
  } 
  else {
    redirect('zmien_haslo.php?error=Proszę wypełnić wszystkie pola.');
  }

?>