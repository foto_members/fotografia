<main>
            <article>
                <h1>Dlaczego fotografia?</h1>
                <p>Odkąd pamiętam aparat zawsze był moim trzecim okiem. Po zakończeniu liceum i zdanej maturze zdecydowałam się na studia fotograficzne. Od kilku lat uczę się oraz doszkalam w kierunku fotografii. Pasja stała się pracą.</p> 
                <p>Moja osobowość w znacznej mierze wpływa na to w jaki sposób widzę i utrwalam na zdjęciach świat, a także wydarzenia, w których dzięki Wam uczestniczę. Najbardziej cenię sobie możliwość zatrzymania wspomnień, ulotnych momentów i osób.</p>
                <p>Te niepowtarzalne chwile pełne Waszych emocji zatrzymanych w kadrze, tworzą najlepszą pamiątkę.</p> 
            </article>
            <aside>
                <section class="widget">
                    <h3>W skrócie o mnie</h3>
                    <img src="images/moje.jpg" alt="???">
                    <p>Nazywam się Martyna Bogdańska. Zawodowo zajmuje się fotografią, hobbystycznie pomagam bezdomnym futrzakom.</p>
                    <p>Moje wykszatłcenie : </p> <ul>
                        <li>Wrocławska Szkoła Fotografii AFA - kierunek Fotografia</li>
                        <li>Akademia Sztuk Pięknych im. Eugeniusza Gepperta w Wrocławiu - kierunek Forografia i Multimedia</li>
                    </ul> 
                </section>
                <section class="widget">
                    <h3>Ostatnia Aktywność</h3>
                    <ul>
                        <li><a href="https://www.facebook.com/martynabogdanskafotografia/posts/1994073530804703"><u>Życzenia Świąteczne</u></a></li>
                        <li><a href="https://www.facebook.com/martynabogdanskafotografia/posts/1993155834229806"><u>Psiaki do Adopcji 4 - kolejna sesja</u></a></li>
                        <li><a href="https://www.facebook.com/martynabogdanskafotografia/posts/1969792843232772"><u>PROdukty, czyli co dostajecie w zamian</u></a></li>
                    </ul>
                </section>
            </aside>
            </main>