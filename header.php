<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Martyna Bogdańska - FOTOGRAFIA</title>
    <link rel="icon" href="favicon.ico">
    <!-- Style-->
    <link rel="stylesheet" href="CSS/Normalize.css">
    <link rel="stylesheet" href="CSS/style.css">
    <link rel="stylesheet" href="CSS/bootstrap.min.css">
    <!-- Font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet">
   <!-- slider-->
     <link rel="stylesheet" href="js/nivo-slider/nivo-slider.css" type="text/css" />  
</head>